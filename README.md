# qaac-linux

## Overview

This project allows you to use the great AAC/ALAC command line encoder qaac under a Linux environment. It is based on
the [Andrew's Corner tutorial](http://www.andrews-corner.org/linux/qaac.html) and includes an script to simply convert
all FLAC files with an easy command.

## Install

First of all you need to install:

- wine
- p7zip
- unzip
- wget

Then clone this repo and run the `install-qaac.sh` sh script. It is compatible with Bash and zsh, but for this last one
you need to edit the file and change `.bashrc` to `.zshrc`. Copy the `convert-flac-to-m4a` file to `$HOME/.local/bin`
(or `/usr/bin/`) and make sure that is on `$PATH`. And run `qaac --check` to verify that is correctly working.

## License

This project is licensed under [The Unlicense](LICENSE).
